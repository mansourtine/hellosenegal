import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-culture',
  templateUrl: './culture.component.html',
  styleUrls: ['./culture.component.css']
})
export class CultureComponent implements OnInit {

  varTest:boolean = true;
  labelButton:string = 'voir suite';

  constructor() { }

  ngOnInit() {
  }

  test() {

    if(this.varTest)
    {
        this.varTest = false;
        this.labelButton = ' fermer ';
    }
    else
    {
        this.varTest = true;
        this.labelButton = 'voir Suite';
    }
}

}
