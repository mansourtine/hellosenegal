import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'senegal'
})
export class SenegalPipe implements PipeTransform {

  /*transform(code: string, args?: any): string {
    return `SN-${code}`;
  }*/

  transform(code: string, pays: string): string {
    
    return `${pays}-${code}`;

  }

}
