import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'plurielPipe'
})
export class PlurielPipePipe implements PipeTransform {

  transform(mot: string, args?: any): string {
    return `${mot}s`;
  }

}
