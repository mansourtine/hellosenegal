import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-slider-carousel',
  templateUrl: './slider-carousel.component.html',
  styleUrls: ['./slider-carousel.component.css']
})
export class SliderCarouselComponent implements OnInit {

  region: string = 'Dakar';

  imgUrl1: string = ('../../assets/images/sn3.jpg');
  imgUrl2: string = ('../../assets/images/sn2.jpg');
  imgUrl3: string = ('../../assets/images/sn4.jpg');


  constructor() { }

  ngOnInit() {
  }

}
