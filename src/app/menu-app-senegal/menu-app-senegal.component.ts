import { Component, OnInit } from '@angular/core';
import { RegionsService } from '../services/regions.service';

@Component({
  selector: 'app-menu-app-senegal',
  templateUrl: './menu-app-senegal.component.html',
  styleUrls: ['./menu-app-senegal.component.css']
})
export class MenuAppSenegalComponent implements OnInit {
  connecte = false

  constructor(private regser: RegionsService){}
  ngOnInit(){
  }
  connexion(){
    this.connecte = this.regser.seConnecter()
  }
  deconnexion(){
    this.connecte = this.regser.seDeconnecter()
  }
}
