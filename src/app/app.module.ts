import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuAppSenegalComponent } from './menu-app-senegal/menu-app-senegal.component';
import { SliderCarouselComponent } from './slider-carousel/slider-carousel.component';
import { EconomieComponent } from './economie/economie.component';
import { ReliefComponent } from './relief/relief.component';
import { CultureComponent } from './culture/culture.component';

import { FormsModule} from '@angular/forms';
import { RechercheComponent } from './recherche/recherche.component';
import { ToggleDirective } from './directive/toggle.directive';
import { PlurielPipePipe } from './mesPipes/pluriel/pluriel-pipe.pipe';
import { SenegalPipe } from './mesPipes/indicatif/senegal.pipe' // pour les formulaires
import {FallserviceService} from './services/fallservice.service';

@NgModule({
  declarations: [
    AppComponent,
    MenuAppSenegalComponent,
    SliderCarouselComponent,
    EconomieComponent,
    ReliefComponent,
    CultureComponent,
    RechercheComponent,
    ToggleDirective,
    PlurielPipePipe,
    SenegalPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [
      FallserviceService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
