import { Component, OnInit } from '@angular/core';
import { RegionsService } from '../services/regions.service';

@Component({
  selector: 'app-relief',
  templateUrl: './relief.component.html',
  styleUrls: ['./relief.component.css']
})
export class ReliefComponent implements OnInit {

  toggle:boolean = false;
  pipNat:string = "Test pipe native";
  mot:string = "Test pipe personalisé";
  

  constructor(private regser: RegionsService){}

  regions: any[]
  
  ngOnInit(){
    this.regions=this.regser.afficheRegion();
  }


  toggleAction(){

    if(this.toggle){
      this.toggle = false;
    }
    else{
      this.toggle = true;
    }
  }
  

}
