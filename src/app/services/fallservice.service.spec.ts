import { TestBed } from '@angular/core/testing';

import { FallserviceService } from './fallservice.service';

describe('FallserviceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FallserviceService = TestBed.get(FallserviceService);
    expect(service).toBeTruthy();
  });
});
