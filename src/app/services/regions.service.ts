import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class RegionsService {

  constructor() { }
  regions:any = [
    {
      code: 'DK',
      nom: 'Dakar',
      sup: '75'
    },
    {
      code: 'TH',
      nom: 'Thies',
      sup: '90'
    },
    {
      code: 'TB',
      nom: 'Tamba',
      sup: '125'
    }
  ];
  afficheRegion(){
    return this.regions;
  }
  seConnecter(){
    alert('connexion reussie')
    return true;
  }
  seDeconnecter(){
    alert('deconnexion reussie')
    return false;
  }
}
