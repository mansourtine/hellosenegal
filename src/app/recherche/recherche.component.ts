import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-recherche',
  templateUrl: './recherche.component.html',
  styleUrls: ['./recherche.component.css']
})
export class RechercheComponent implements OnInit {

  recherche: string;
  imge: string;

  constructor() { 
    this.imge = '';
  }

  ngOnInit() {
  }

  test(){
    
    //console.log(this.recherche);

    if(this.recherche == 'dakar'){
      this.imge = ('../../assets/images/sn4.jpg');
    }
    else  if(this.recherche == 'thies'){
      this.imge = ('../../assets/images/thies2.jpg');
    }
    else
      this.imge = '';

  }

}
