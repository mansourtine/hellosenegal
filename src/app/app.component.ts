import { Component } from '@angular/core';
import { RegionsService } from './services/regions.service';

@Component({
  selector: 'app-root',
  providers: [RegionsService],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'hello©senegal';
  
}
