import { Directive, ElementRef, Renderer, HostListener } from '@angular/core';

@Directive({
  selector: '[appToggle]'
})
export class ToggleDirective {

  constructor(private param1: ElementRef, private param2: Renderer) { 

    param2.setElementStyle(param1.nativeElement, 'color', 'red')
    param2.setElementStyle(param1.nativeElement, 'font-size', '50px')
    
  }

  @HostListener('mouseenter') onMouseEnter() {
    this.highlight('yellow');
  }
  
  @HostListener('mouseleave') onMouseLeave() {
    this.highlight(null);
  }
  
  private highlight(color: string) {
    this.param1.nativeElement.style.backgroundColor = color;
  }

}
